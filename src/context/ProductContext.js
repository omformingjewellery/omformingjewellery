import { createContext, useContext, useState, useEffect } from "react";
import { products } from "../data/products";

const ProductContext = createContext();

const ProductProvider = ({ children }) => {
  const [productsData, setProductsData] = useState(products);
  const [filter, setFilter] = useState("all");
  const [isOpen, setIsOpen] = useState(false);
  const [isHovered, setIsHovered] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showProductMenu, setShowProductMenu] = useState(false);

  useEffect(() => {
    setLoading(true);
    if (filter === "all") {
      setLoading(false);
      return setProductsData(products);
    } else {
      let newproducts = products?.filter(
        (item) => item.category === filter.toLowerCase()
      );

      if (newproducts) {
        setProductsData(newproducts);
        setLoading(false);
      }
    }
  }, [filter]);

  useEffect(() => {
    const savedCategory = localStorage.getItem("category");
    if (savedCategory) {
      setFilter(savedCategory);
      setLoading(false);
    }
  }, []);

  return (
    <ProductContext.Provider
      value={{
        productsData,
        filter,
        setFilter,
        isOpen,
        setIsOpen,
        isHovered,
        setIsHovered,
        loading,
        setLoading,
        showProductMenu,
        setShowProductMenu
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};

const useProducts = () => useContext(ProductContext);

export { useProducts, ProductProvider };
