export const testimonials = [
  {
    name: "Jay Soni",
    text: "The craftsmanship of their jewellery is unmatched! I bought a stunning pendant for my sister, and she absolutely loves it. Their dedication to quality is truly commendable.",
    rating: 5,
  },
  {
    name: "Hitesh",
    text: "I'm impressed by their innovative designs and attention to detail. The custom ring I ordered for my engagement was perfect. Highly recommended for anyone looking for unique pieces!",
    rating: 4,
  },
  {
    name: "Viru Sharma",
    // company: "Professor",
    text: "Their customer service is top-notch. They helped me choose the perfect pair of earrings for my daughter's graduation. They always go above and beyond to meet their customers' needs.",
    rating: 4,
  },
];
