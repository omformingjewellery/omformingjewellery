export const teamMembers = [
  {
    image:
      "https://cdn.pixabay.com/photo/2015/01/08/18/30/entrepreneur-593371_1280.jpg",
    alt: "Team member 1",
    description:
      "OM Forming Jewellery, located at Palanpur, Gujarat - 385001, is a highly reputed jewellery shop renowned for its exquisite collection and exceptional craftsmanship. Over the years, it has earned the trust and admiration of customers for its dedication to quality and service. Whether you're looking for traditional designs or contemporary pieces, OM Forming Jewellery offers a wide range of options to suit every taste and occasion, making it a preferred destination for jewellery enthusiasts in the region.",
  },
  // {
  //   image:
  //     "https://cdn.pixabay.com/photo/2015/01/08/18/30/entrepreneur-593371_1280.jpg",
  //   alt: "Team member 2",
  //   description:
  //     "Dolorem dicta deserunt perspiciatis molestias maxime commodi molestiae quisquam iusto sit, impedit odit soluta illum incidunt blanditiis mollitia ipsam, atque recusandae eos nobis totam.",
  // },
];
