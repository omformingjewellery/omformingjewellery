import { Roboto, Roboto_Condensed, Roboto_Slab } from "next/font/google";
export const robotoCondensed = Roboto_Condensed({
  subsets: ["latin"],
  weight: ["400", "700"],
});
export const robotoSlab = Roboto_Slab({
  subsets: ["latin"],
  weight: ["400", "700"],
});

// common button style
export const ctaButtonStyle = {
  backgroundColor: "#0A192F",
  color: "#ffffff",
};

export const headingStyle = {
  color: "#333333",
};
export const bodyText = {
  //   color: "#333333",
  color: "#536872",
  fontSize: "1rem",
};
