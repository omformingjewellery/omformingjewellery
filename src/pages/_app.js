import "@/styles/globals.css";
import { ProductProvider } from "../context/ProductContext";

export default function App({ Component, pageProps }) {
  // console.log(Component, pageProps);
  return (
    <ProductProvider>
      <Component {...pageProps} />
    </ProductProvider>
  );
}
