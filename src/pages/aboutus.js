import React from "react";
import { Roboto } from "next/font/google";
import { Card, CardHeader, CardContent } from "@/components/ui/card";
import { StarIcon } from "lucide-react";
import Header from "@/components/Header/Header";
import { teamMembers } from "@/data/teamMembers";
import WhatsAppButton from "@/components/Floatingbutton/FloatingButton";
import { testimonials } from "@/data/testimonials";

const roboto = Roboto({ subsets: ["latin"], weight: ["400", "700"] });

const AboutUs = () => {
  const renderStars = (rating) => {
    return Array(5)
      .fill(0)
      .map((_, i) => (
        <StarIcon
          key={i}
          className={`w-5 h-5 ${
            i < rating ? "text-yellow-400" : "text-gray-300"
          }`}
        />
      ));
  };

  return (
    <main
      className={`min-h-screen flex flex-col items-center justify-start gap-12 p-4 sm:p-8 ${roboto.className}`}
    >
      <Header />
      <section className="w-full max-w-4xl">
        <h1 className="text-3xl font-bold mb-6 text-center">Who We Are</h1>
        <div className="grid grid-cols-1 md:grid-cols-1 gap-6">
          {teamMembers.map((member, index) => (
            <Card key={index} className="overflow-hidden">
              <CardContent className="p-0">
                {/* <Image
                  src={member.image}
                  alt={member.alt}
                  width={500}
                  height={300}
                  className="w-full h-48 object-cover"
                /> */}
              </CardContent>
              <CardHeader>
                <p className="text-sm text-gray-600">{member.description}</p>
              </CardHeader>
            </Card>
          ))}
        </div>
      </section>

      <section className="w-full max-w-4xl">
        <h2 className="text-2xl font-bold mb-6 text-center">
          What Our Clients Say
        </h2>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 ">
          {testimonials.map((testimonial, index) => (
            <Card key={index} className="flex flex-col justify-between">
              <CardHeader>
                <div className="flex justify-between items-center mb-2">
                  <div>
                    <h3 className="font-semibold">{testimonial.name}</h3>
                    <p className="text-sm text-gray-500">
                      {testimonial.company}
                    </p>
                  </div>
                  <div className="flex">{renderStars(testimonial.rating)}</div>
                </div>
              </CardHeader>
              <CardContent>
                <p className="text-sm italic">"{testimonial.text}"</p>
              </CardContent>
            </Card>
          ))}
        </div>
      </section>
      <WhatsAppButton />
    </main>
  );
};

export default AboutUs;
