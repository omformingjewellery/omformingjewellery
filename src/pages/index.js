import { Roboto, Roboto_Condensed } from "next/font/google";
import { CarouselPlugin } from "@/components/Carousel/Carousel";
import { headingStyle, robotoSlab } from "@/styles/commonStyles";
import CategoryCard from "@/components/CategoryCard/CategoryCard";
import Footer from "@/components/Footer/Footer";
import Header from "@/components/Header/Header";
import WhatsAppButton from "@/components/Floatingbutton/FloatingButton";
import { categories } from "@/constants";
import { useState, useEffect } from "react";

const roboto = Roboto({ subsets: ["latin"], weight: ["400", "700"] });

export default function Home() {
  const [randomCategories, setRandomCategories] = useState([]);

  useEffect(() => {
    const shuffledCategories = [...categories];
    for (let i = shuffledCategories.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [shuffledCategories[i], shuffledCategories[j]] = [
        shuffledCategories[j],
        shuffledCategories[i],
      ];
    }
    setRandomCategories(shuffledCategories.slice(0, 5));
  }, [categories]);

  return (
    <main
      className={`flex min-h-screen flex-col items-center gap-8 p-4 sm:p-8 ${roboto.className}`}
    >
      <Header />
      <CarouselPlugin />
      <div className="w-full  flex flex-col items-center justify-center gap-6 px-4">
        <h1
          className={`uppercase text-center ${robotoSlab.className} text-4xl max-[450px]:text-lg`}
          style={headingStyle}
        >
          Shop by categories
        </h1>
        <div className="w-12/12 flex flex-wrap justify-center gap-4  bg-slate-100 p-4 rounded-md max-[450px]:flex-wrap">
          {categories?.map((category) => (
            <CategoryCard key={category.name} category={category} />
          ))}
        </div>
      </div>
      {/* <div className="w-full  flex flex-col items-center justify-center gap-6 px-4">
        <h1
          className={`uppercase text-center ${robotoSlab.className} text-4xl max-[450px]:text-lg`}
          style={headingStyle}
        >
          Shop by Popularity
        </h1>
        <div className="w-12/12 flex flex-wrap justify-center gap-4  bg-slate-100 p-4 rounded-md max-[450px]:flex-wrap">
          {categories?.map((category) => (
            <CategoryCard key={category.name} category={category} />
          ))}
        </div>
      </div> */}
      <WhatsAppButton />
      <Footer />
    </main>
  );
}
