import { Roboto } from "next/font/google";
import { ProductCard } from "@/components/ProductCard/ProductCard";
import { useProducts } from "@/context/ProductContext";
import WhatsAppButton from "@/components/Floatingbutton/FloatingButton";
import { useState, useEffect } from "react";
import Header from "@/components/Header/Header";
import { CustomPagination } from "@/components/Pagination/Pagination";
import { SkeletonCard } from "@/components/Loader/Loader";

const roboto = Roboto({ subsets: ["latin"], weight: ["400", "700"] });

export default function ProductPage() {
  const { productsData, filter, loading } = useProducts();
  const dataperPage = 20;
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    if (productsData?.length > 0) {
      setTotalPages(Math.ceil(productsData.length / dataperPage));
    } else {
      setTotalPages(0);
    }
  }, [productsData, dataperPage, filter]);

  const endIndex = dataperPage * currentPage;
  const startIndex = endIndex - dataperPage;
  const paginatedProductData = productsData?.slice(startIndex, endIndex);

  return (
    <main
      className={`flex min-h-screen flex-col items-center gap-4 p-4 sm:p-8 ${roboto.className}`}
    >
      <Header />
      <div className="grid grid-cols-4 gap-4 max-[450px]:grid-cols-1 max-[930px]:grid-cols-2 max-[1290px]:grid-cols-3">
        {loading
          ? paginatedProductData?.map((product) => (
              <SkeletonCard key={product.id} />
            ))
          : paginatedProductData?.map((product) => (
              <ProductCard key={product.id} product={product} />
            ))}
      </div>
      <WhatsAppButton />
      {totalPages > 1 && (
        <CustomPagination
          totalPages={totalPages}
          setCurrentPage={setCurrentPage}
          currentPage={currentPage}
        />
      )}
    </main>
  );
}
