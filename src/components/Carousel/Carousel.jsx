import * as React from "react";
import Autoplay from "embla-carousel-autoplay";
import { Card, CardContent } from "@/components/ui/card";
import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from "@/components/ui/carousel";
import { carouselsImages } from "@/constants";
import Image from "next/image";
import { useProducts } from "@/context/ProductContext";

export function CarouselPlugin() {
  const plugin = React.useRef(
    Autoplay({ delay: 1500, stopOnInteraction: true })
  );
  const { setIsOpen } = useProducts();

  return (
    <Carousel
      plugins={[plugin.current]}
      className="w-11/12 max-w-max max-[450px]:w-11/12 max-[560px]:w-10/12"
      onMouseEnter={plugin.current.stop}
      onMouseLeave={plugin.current.reset}
    >
      <CarouselContent className="h-3/5 ">
        {carouselsImages.map((img, index) => (
          <CarouselItem key={index}>
            <div className="p-1 relative">
              <Card onClick={() => setIsOpen(false)}>
                <CardContent className="flex items-center justify-center p-2 relative ">
                  <Image
                    width={1200}
                    height={400}
                    src={img}
                    alt={`Carousel image ${index + 1}`}
                    style={{
                      objectFit: "cover",
                      // height: "500px",
                      width: "100%",
                    }}
                    className=" w-full h-96 max-[450px]:h-72"
                  />
                  {/* <div className="absolute inset-0 flex items-center left-20 top-48 max-[450px]:left-10">
                    <Button className={robotoCondensed} style={ctaButtonStyle}>
                      Send Message
                    </Button>
                  </div> */}
                </CardContent>
              </Card>
            </div>
          </CarouselItem>
        ))}
      </CarouselContent>
      <CarouselPrevious className="max-[450px]:hidden" />
      <CarouselNext className="max-[450px]:hidden" />
    </Carousel>
  );
}
