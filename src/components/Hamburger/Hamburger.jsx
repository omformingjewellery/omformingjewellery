import Link from "next/link";
import { useState, useEffect, useRef } from "react";
import { usePathname, useRouter } from "next/navigation";
import { useProducts } from "@/context/ProductContext";

const Hamburger = () => {
  const router = useRouter();
  const {
    filter,
    setFilter,
    isOpen,
    setIsOpen,
    showProductMenu,
    setShowProductMenu,
  } = useProducts();
  const currentPath = usePathname();
  const menuRef = useRef(null);

  const toggleProductMenu = () => {
    setShowProductMenu(!showProductMenu);
  };

  const handleCloseMenu = () => {
    setIsOpen(false);
    setShowProductMenu(false);
  };
  const handleScroll = () => {
    if (isOpen) {
      setIsOpen(false);
    }
  };

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (menuRef.current && !menuRef.current.contains(event.target)) {
        handleCloseMenu();
      }
    };

    window.addEventListener("scroll", handleScroll);
    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      window.removeEventListener("scroll", handleScroll);
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [isOpen]);

  return (
    <div
      ref={menuRef}
      className="absolute right-1 humburger-menu flex flex-col items-center
      w-7/12 h-full bg-white min-[550px]:hidden"
    >
      <ul className="flex flex-col gap-8 text-[18px]  w-9/12">
        <li
          className={`hover:text-sky-800 font-normal transition border-b-2 border-white hover:border-sky-800 cursor-pointer ${
            currentPath === "/" ? "text-gray-900 font-extrabold" : ""
          }`}
        >
          <Link
            href="/"
            onClick={() => {
              setFilter("all");
              handleCloseMenu();
            }}
          >
            Home
          </Link>
        </li>
        <div className="group relative">
          <Link
            href={"/productpage"}
            onClick={toggleProductMenu}
            className={` transition border-b-2 border-white  cursor-pointer ${
              currentPath === "/productpage"
                ? "text-gray-900 font-extrabold"
                : ""
            }`}
          >
            Product
          </Link>
          {showProductMenu && (
            <div className="w-full mt-7 ml-3 bg-white text-black duration-500 ease-in-out max-h-64 overflow-y-auto">
              <div className="grid grid-cols-1 gap-2">
                {[
                  { category: "all" },
                  { category: "long necklace" },
                  { category: "short necklace" },
                  { category: "earrings" },
                  { category: "beads necklace" },
                  { category: "diamond necklace" },
                  { category: "arbi necklace" },
                  { category: "chain" },
                  { category: "oxidized necklace" },
                  { category: "pendant necklace" },
                ].map((group, index) => (
                  <div key={index} className="flex flex-col">
                    <h3
                      className={`${
                        filter === group.category
                          ? "font-bold text-black"
                          : "text-sky-800"
                      } mb-4 text-[15px] cursor-pointer hover:text-sky-950 hover:border-sky-950 capitalize`}
                      onClick={() => {
                        setFilter(group.category);
                        localStorage.setItem("category", group.category);
                        router.push("/productpage");
                        setIsOpen(false);
                      }}
                    >
                      {group.category}
                    </h3>
                    {/* {group.items.map((item, idx) => (
              <Link
                key={idx}
                href="#"
                className="hover:underline hover:text-sky-800"
              >
                {item}
              </Link>
            ))} */}
                  </div>
                ))}
              </div>
            </div>
          )}
        </div>

        <li
          className={` transition  border-white  cursor-pointer ${
            currentPath === "/aboutus" ? "text-gray-900 font-extrabold" : ""
          }`}
        >
          <div
            onClick={() => {
              router.push("/aboutus");
              handleCloseMenu();
            }}
          >
            <span>About Us</span>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default Hamburger;
