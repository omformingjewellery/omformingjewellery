import { Skeleton } from "@/components/ui/skeleton";

export function SkeletonCard({ height, width }) {
  return (
    <div className="">
      <Skeleton className={`h-[390px] w-[320px] rounded-xl`} />
    </div>
  );
}
