import React from "react";
import { IoLogoWhatsapp } from "react-icons/io";

const WhatsAppButton = () => {
  // const handleWhatsAppClick = () => {
  //   // Replace 'YOUR_PHONE_NUMBER' with your actual WhatsApp number including the country code
  //   window.open(
  //     "https://wa.me/9714628228?text=Hello Omforming jewellery i want to purchase this thing",
  //     "_blank"
  //   );
  // };

  return (
    <a
      className="fixed bottom-20 left-4 bg-green-500 hover:bg-green-600 text-white p-4 rounded-full cursor-pointer shadow-lg max-[450px]: bottom-5 left-3 z-50"
      href="https://wa.me/9875207601?text=Hello%20OM%20FORMING%20JEWELLERY%2C%20I%20am%20very%20interested%20in%20your%20products%20and%20would%20like%20to%20learn%20more%20about%20them.%20Thank%20you!"
      target="_blank"
    >
      <IoLogoWhatsapp size={24} />
    </a>
  );
};

export default WhatsAppButton;
