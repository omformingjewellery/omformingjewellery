import Image from "next/image";
import React from "react";
import { IoMdClose } from "react-icons/io";

const HoverImage = ({ product, setIsHovered }) => {
  return (
    <div className="absolute z-50">
      <Image
        src={product.img}
        alt={product.name}
        className="rounded-lg relative w-[300px]  h-[470px] max-[450px]:w-[320px]"
        width={300}
        height={460}
        style={{ height: "500px", objectFit: "cover" }}
      />
      <IoMdClose
        onClick={() => setIsHovered(null)}
        className="absolute top-3 right-0 cursor-pointer max-[450px]:right-6 "
        size={40}
      />
    </div>
  );
};

export default HoverImage;
