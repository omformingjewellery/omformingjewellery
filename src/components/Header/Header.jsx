import Link from "next/link";
import React, { useState } from "react";
import { Roboto } from "next/font/google";
import {
  robotoCondensed,
  robotoSlab,
  headingStyle,
} from "@/styles/commonStyles";
import Image from "next/image";
import { FaBars } from "react-icons/fa6";
import Hamburger from "../Hamburger/Hamburger";
import { useRouter } from "next/navigation";
import { useProducts } from "../../context/ProductContext";
import { IoMdClose } from "react-icons/io";

const roboto = Roboto({ subsets: ["latin"], weight: ["400", "700"] });

const Header = () => {
  const router = useRouter();
  const { filter, setFilter, isOpen, setIsOpen, isHovered, setIsHovered } =
    useProducts();

  return (
    <nav className="w-11/12 z-50">
      <div className="h-10vh flex justify-between px-5 lg:py-5">
        <div className="flex items-center flex-1 italic cursor-pointer gap-4">
          <Image
            height={80}
            width={80}
            src="https://ik.imagekit.io/omformingjewellery/logo/new_logo.jpg?updatedAt=1722415401472"
            alt="om froming jewellery"
          />
          <h2
            className={`text-3xl font-semibold ${robotoSlab.className} max-[450px]:text-sm  max-[780px]:text-sm`}
            style={headingStyle}
            onClick={() => {
              router.push("/");
              setFilter("all");
              setIsOpen(false);
            }}
          >
            OM Forming Jewellery
          </h2>
        </div>
        <div className="flex items-center max-[450px]:hidden max-[560px]:hidden">
          <ul className="flex gap-8 text-[18px]">
            <div
              onClick={() => {
                router.push("/");
                setFilter("all");
              }}
            >
              <li
                className={`hover:text-sky-800 transition border-b-2 border-white hover:border-sky-800 cursor-pointer ${robotoCondensed.className}`}
              >
                Home
              </li>
            </div>
            <div className="group">
              <button
                className={`hover:text-sky-800 transition border-b-2 border-white hover:border-sky-800 cursor-pointer ${robotoCondensed.className}`}
                onClick={() => {
                  router.push("/productpage");
                  // setFilter("all");
                }}
              >
                Product
              </button>
              <div className="invisible group-hover:visible group-hover:flex flex-col absolute right-40 h-fit top-28 p-8 w-fit bg-white z-20 text-black duration-300">
                <div className=" grid grid-cols-2 gap-x-8">
                  {[
                    {
                      category: "all",
                      // items: ["Pendant", "Choker", "Locket", "Chain"],
                    },
                    {
                      category: "long necklace",
                      // items: ["Pendant", "Choker", "Locket", "Chain"],
                    },
                    {
                      category: "short necklace",
                      // items: ["Solitaire", "Cocktail", "Eternity", "Signet"],
                    },
                    {
                      category: "earrings",
                      // items: ["Studs", "Hoops", "Drops", "Chandeliers"],
                    },
                    {
                      category: "beads necklace",
                      // items: ["Bangle", "Charm", "Tennis", "Cuff"],
                    },
                    {
                      category: "diamond necklace",
                      // items: ["Bangle", "Charm", "Tennis", "Cuff"],
                    },
                    {
                      category: "arbi necklace",
                      // items: ["Bangle", "Charm", "Tennis", "Cuff"],
                    },
                    {
                      category: "chain",
                      // items: ["Bangle", "Charm", "Tennis", "Cuff"],
                    },
                    {
                      category: "oxidized necklace",
                      // items: ["Bangle", "Charm", "Tennis", "Cuff"],
                    },
                    {
                      category: "pendant necklace",
                      // items: ["Bangle", "Charm", "Tennis", "Cuff"],
                    },
                  ].map((group, index) => (
                    <div key={index} className="flex flex-col ">
                      <h3
                        className={`${
                          filter === group.category
                            ? "font-bold text-black"
                            : " text-sky-800 "
                        } mb-4 text-md cursor-pointer  hover:text-sky-950 hover:border-sky-950 hover:border-b-2 capitalize`}
                        onClick={() => {
                          setFilter(group.category);
                          router.push("/productpage");
                          localStorage.setItem("category", group.category);
                        }}
                      >
                        {group.category}
                      </h3>
                      {/* {group.items.map((item, idx) => (
                        <Link
                          key={idx}
                          href="#"
                          className="hover:underline hover:text-sky-800"
                        >
                          {item}
                        </Link>
                      ))} */}
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <Link href="/aboutus">
              <li
                className={`hover:text-sky-800 transition border-b-2 border-white hover:border-sky-800 cursor-pointer ${robotoCondensed.className}`}
              >
                About Us
              </li>
            </Link>
          </ul>
        </div>
        <div className="menu-icon absolute top-12 right-8  min-[560px]:hidden ">
          {isOpen ? (
            <IoMdClose
              onClick={() => {
                setIsOpen((prev) => !prev);
                if (isHovered) setIsHovered(false);
              }}
              size={20}
            />
          ) : (
            <FaBars
              onClick={() => {
                setIsOpen((prev) => !prev);
                if (isHovered) setIsHovered(false);
              }}
            />
          )}
        </div>
      </div>
      {isOpen && <Hamburger />}
    </nav>
  );
};

export default Header;
