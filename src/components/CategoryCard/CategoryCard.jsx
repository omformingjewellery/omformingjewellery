"use client";
import * as React from "react";
import { Card, CardHeader } from "@/components/ui/card";
import Image from "next/image";
import { bodyText, headingStyle, robotoCondensed } from "@/styles/commonStyles";
import { useRouter } from "next/navigation";
import { useProducts } from "@/context/ProductContext";

export default function CategoryCard({ category }) {
  const { filter, setFilter, setIsOpen } = useProducts();
  const router = useRouter();
  const setCategoryHandler = (category) => {
    setFilter(category.name);
    router.push("/productpage");
  };

  return (
    <div>
      <Card
        className="w-[250px] cursor-pointer"
        onClick={() => {
          setCategoryHandler(category);
          setIsOpen(false);
        }}
      >
        {/* Card Header with Image */}
        <CardHeader className="flex items-center justify-center">
          <Image
            src={category.img}
            alt={category.name}
            className="w-full rounded-lg "
            width={150}
            height={150}
            style={{ height: "180px", width: "180px" }}
          />
        </CardHeader>
      </Card>
      <div className="flex items-center justify-center">
        <h6 style={bodyText} className={`${robotoCondensed} capitalize mt-2`}>
          {category.name}
        </h6>
      </div>
    </div>
  );
}
