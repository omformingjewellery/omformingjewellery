import React, { useState } from "react";
import { Card, CardHeader, CardContent } from "@/components/ui/card";
import Image from "next/image";
import { bodyText, headingStyle, robotoCondensed } from "@/styles/commonStyles";
import HoverImage from "../HoverImage/HoverImage";
import { useProducts } from "@/context/ProductContext";

export function ProductCard({ product }) {
  const { isOpen, setIsOpen, isHovered, setIsHovered } = useProducts();

  const baseMessage = `Hello OM FORMING JEWELLERY, I am interested to purchase product with id ${product?.id}, the category is ${product?.category} and the price is ${product?.price} Rs. , It would be great if you tell me how can i purchase.`;
  const encodedMessage = encodeURIComponent(baseMessage);

  return (
    <div className="p-4 flex flex-col">
      <Card className="w-[300px] h-auto cursor-pointer shadow-lg rounded-lg overflow-hidden max-[450px]:w-[350px] max-[360px]:w-[300px] max-w-[320px] ">
        <CardHeader
          className="flex items-center justify-center bg-gray-100 p-4"
          onClick={() => {
            setIsHovered(product);
            setIsOpen(false);
          }}
        >
          <Image
            src={product.img}
            alt={product.category}
            className="rounded-lg "
            width={300}
            height={150}
            // style={{ height: "150px", width: "100%", objectFit: "cover" }}
          />
        </CardHeader>

        <CardContent className="p-4">
          <h5
            style={headingStyle}
            className={`${robotoCondensed} text-xl mb-2 max-[450px]:text-lg`}
          >
            Product code: {product.id}
          </h5>
          <h5
            style={bodyText}
            className={`${robotoCondensed} text-sm text-gray-600 mb-4 capitalize`}
          >
            Category: {product.category}
          </h5>
          <p
            style={bodyText}
            className={`${robotoCondensed} text-sm text-gray-600 mb-4`}
          >
            Price: {product.price} Rs.
          </p>

          <div className="flex justify-center items-center">
            <a
              className="bg-black text-center text-white text-md px-4 py-2 w-full rounded-md hover:bg-gray-700"
              href={`https://wa.me/9875207601?text=${encodedMessage}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              Send Inquiry
            </a>
          </div>
        </CardContent>
      </Card>
      {isHovered && isHovered?.id === product?.id && (
        <HoverImage product={product} setIsHovered={setIsHovered} />
      )}
    </div>
  );
}
