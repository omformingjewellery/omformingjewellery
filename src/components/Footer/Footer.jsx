import Link from "next/link";
import React, { useState, useEffect } from "react";
import { FaFacebook } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa6";
import { IoLocationSharp } from "react-icons/io5";
import { useRouter } from "next/router";
import { useProducts } from "@/context/ProductContext";

const Footer = () => {
  const { filter, setFilter } = useProducts();
  const [isClient, setIsClient] = useState(false);
  const router = useRouter();

  const setCategoryHandler = (category) => {
    setFilter(category);
    router.push("/productpage");
    localStorage.setItem("category", category);
  };

  useEffect(() => {
    setIsClient(true);
  }, []);

  if (!isClient) {
    return null;
  }
  return (
    <footer>
      <div className="max-w-screen-xl px-4 py-12 mx-auto space-y-8 overflow-hidden sm:px-6 lg:px-8">
        <h2 className="text-center text-xl font-bold text-gray-700">
          Shop with us
        </h2>
        <nav className="flex flex-wrap justify-center -mx-5 -my-2 cursor-pointer">
          {[
            "long necklace",
            "short necklace",
            "arbi necklace",
            "earrings",
            "diamond necklace",
            "beads necklace",
            "chain",
            "oxidized necklace",
            "pendant necklace",
          ].map((item) => (
            <div className="px-5 py-2" key={item}>
              <div
                onClick={() => setCategoryHandler(item)}
                className="text-base leading-6 text-gray-500 hover:text-gray-900 capitalize"
              >
                {item}
              </div>
            </div>
          ))}
        </nav>
        {/*  Map code */}

        {/* <div style={{ width: "100%" }}>
          <iframe
            title="OM Forming Jewellery Location"
            width="100%"
            height="600"
            frameBorder="0"
            scrolling="no"
            marginHeight="0"
            marginWidth="0"
            src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=4/2246%20Choksi%20Vas,%20Tower%20Road,%20Moti%20Bazar,%20Palanpur,%20Gujarat%20385001+(OM%20FORMING%20JEWELLERY)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
          >
            <a href="https://www.gps.ie/">gps systems</a>
          </iframe>
        </div> */}
        {/* map code ends here */}

        <h2 className="text-center text-xl font-bold text-gray-700 mt-8">
          REACH US
        </h2>
        <div className="flex justify-center mt-8 space-x-6">
          {[
            {
              name: <FaFacebook className="w-6 h-6" />,
              label: "Facebook",
              url: "https://www.facebook.com/profile.php?id=61562111648650",
            },
            {
              name: <FaInstagram className="w-6 h-6" />,
              label: "Instagram",
              url: "https://www.instagram.com/omformingjewellery?igsh=MjB4N2JvOTUxYjA2",
            },
            // {
            //   name: <IoLocationSharp className="w-6 h-6" />,
            //   label: "Map",
            //   url: "",
            // },
          ].map((icon, index) => (
            <Link
              href={icon?.url}
              className="text-gray-400 hover:text-blue-500"
              key={index}
            >
              <span className="sr-only">{icon.label}</span>
              {icon.name}
            </Link>
          ))}
        </div>
        <p className="mt-2 text-base leading-6 text-center text-gray-400">
          Email us on: omformingjewellery@gmail.com
          <br />
          <br />
          Contact No: +919875207601
          <br />
          Timings : IST 9.30 AM to 8 PM
          <br />
          <br />
          Located at Palanpur, Gujarat - 385001
        </p>
        <p className="mt-8 text-base leading-6 text-center text-gray-400">
          © 2024 OM Forming Jewellery, Inc. All rights reserved.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
