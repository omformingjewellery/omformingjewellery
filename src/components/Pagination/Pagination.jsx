import {
  Pagination,
  PaginationContent,
  PaginationEllipsis,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from "@/components/ui/pagination";

export function CustomPagination({ currentPage, setCurrentPage, totalPages }) {
  const pagesToShow = 4;
  const maxLeft = Math.max(1, currentPage - Math.floor(pagesToShow / 2));
  const maxRight = Math.min(
    totalPages,
    currentPage + Math.floor(pagesToShow / 2) //(6,1+2)=3
  );

  const pageNumbers = [];

  for (let i = 1; i <= totalPages; i++) {
    if (i <= 2 || i >= totalPages - 1 || (i >= maxLeft && i <= maxRight)) {
      pageNumbers.push(i);
    } else if (pageNumbers[pageNumbers.length - 1] !== "...") {
      pageNumbers.push("...");
    }
  }
  return (
    <Pagination>
      <PaginationContent className="cursor-pointer">
        <PaginationItem>
          <PaginationPrevious
            onClick={() =>
              currentPage > 1 && setCurrentPage((prevPage) => prevPage - 1)
            }
            className={currentPage === 1 && "cursor-not-allowed"}
          />
        </PaginationItem>
        {pageNumbers.map((page, index) => (
          <PaginationItem key={index}>
            {page === "..." ? (
              <PaginationEllipsis />
            ) : (
              <PaginationLink
                isActive={page === currentPage}
                onClick={() => setCurrentPage(page)}
              >
                {page}
              </PaginationLink>
            )}
          </PaginationItem>
        ))}
        <PaginationItem>
          <PaginationNext
            onClick={() =>
              currentPage < totalPages &&
              setCurrentPage((prevPage) => prevPage + 1)
            }
            className={currentPage === totalPages && "cursor-not-allowed"}
          />
        </PaginationItem>
      </PaginationContent>
    </Pagination>
  );
}
