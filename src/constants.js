export const carouselsImages = [
  "https://ik.imagekit.io/omformingjewellery/1%20GRM/SONL1008.png?updatedAt=1721907470825",
  "https://ik.imagekit.io/omformingjewellery/Necklace/DNL2303.png?updatedAt=1721907605912",
  "https://ik.imagekit.io/omformingjewellery/Necklace/DNL2301.png?updatedAt=1721907603081",
  "https://ik.imagekit.io/omformingjewellery/1%20GRM/PC1004.png?updatedAt=1721907465901",
];

export const categories = [
  {
    name: "chain",
    img: "https://ik.imagekit.io/omformingjewellery/1%20GRM/PC1004.png?updatedAt=1721907465901",
  },
  {
    name: "long necklace",
    img: "https://ik.imagekit.io/omformingjewellery/1%20GRM/LONL3006.png?updatedAt=1721907464863",
  },
  {
    name: "short necklace",
    img: "https://ik.imagekit.io/omformingjewellery/1%20GRM/SONL1001.png?updatedAt=1721907468830",
  },
  {
    name: "earrings",
    img: "https://ik.imagekit.io/omformingjewellery/Oxidized%20Earrings/Selected/ERG702.png?updatedAt=1721907531380",
  },
  {
    name: "beads necklace",
    img: "https://ik.imagekit.io/omformingjewellery/Necklace/BNL649.png?updatedAt=1721907603323",
  },
  {
    name: "diamond necklace",
    img: "https://ik.imagekit.io/omformingjewellery/Necklace/DNL2305.png?updatedAt=1721907606533",
  },
  {
    name: "arbi necklace",
    img: "https://ik.imagekit.io/omformingjewellery/1%20GRM/AONL7009.png?updatedAt=1721907459786",
  },
  {
    name: "oxidized necklace",
    img: "https://ik.imagekit.io/omformingjewellery/Necklace/OXNL904.png?updatedAt=1721907607393",
  },
  {
    name: "pendant necklace",
    img: "https://ik.imagekit.io/omformingjewellery/1%20GRM/PNL504.png?updatedAt=1721907468103",
  },

  // {
  //   name: "bracelets",
  //   img: "https://cdn.pixabay.com/photo/2016/01/13/08/49/cartier-1137401_640.jpg",
  // },
];
